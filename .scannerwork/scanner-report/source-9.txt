package co.edu.uniajc.project.management.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tasks")
public class TaskModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    // TODO - FK
    @Column(name = "id_requirement")
    private Long idRequirement;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "final_date")
    private Date finalDate;

    @Column(name = "priority")
    private String priority;

    @Column(name = "status")
    private String status;

    @Column(name = "observations")
    private String observations;

    // TODO - FK
    @Column(name = "id_responsible")
    private Long idResponsible;

    public TaskModel() {

    }

    public TaskModel(Long id, Long idRequirement, String name, String description, Date finalDate, String priority, String status, String observations, Long idResponsible) {
        this.id = id;
        this.idRequirement = idRequirement;
        this.name = name;
        this.description = description;
        this.finalDate = finalDate;
        this.priority = priority;
        this.status = status;
        this.observations = observations;
        this.idResponsible = idResponsible;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(Long idRequirement) {
        this.idRequirement = idRequirement;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Long getIdResponsible() {
        return idResponsible;
    }

    public void setIdResponsible(Long idResponsible) {
        this.idResponsible = idResponsible;
    }
}

