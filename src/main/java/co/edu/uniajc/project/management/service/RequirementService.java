package co.edu.uniajc.project.management.service;

import co.edu.uniajc.project.management.model.RequirementModel;
import co.edu.uniajc.project.management.repository.RequirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RequirementService {
    private final RequirementRepository requirementRepository;

    @Autowired
    public RequirementService(RequirementRepository requirementRepository) {
        this.requirementRepository = requirementRepository;
    }

    public List<RequirementModel> findAll() {
        return requirementRepository.findAll();
    }

    public RequirementModel store(RequirementModel project) {
        return requirementRepository.save(project);
    }

    public Optional<RequirementModel> show(Long id) {
        return requirementRepository.findById(id);
    }

    public RequirementModel update(RequirementModel project) {
        return requirementRepository.save(project);

    }

    public int countRequirementTasks(Long id) {
        return requirementRepository.countRequirementTasks(id);
    }

    public boolean delete(Long id) {
        requirementRepository.deleteById(id);

        return true;
    }
}
