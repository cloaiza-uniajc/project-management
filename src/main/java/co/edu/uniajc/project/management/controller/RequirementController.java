package co.edu.uniajc.project.management.controller;

import co.edu.uniajc.project.management.model.RequirementModel;
import co.edu.uniajc.project.management.service.RequirementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/requirement")
@Api("Project")
public class RequirementController {
    private RequirementService requirementService;

    @Autowired
    public RequirementController(RequirementService requirementService) {
        this.requirementService = requirementService;
    }

    @GetMapping(path = "/")
    @ApiOperation(value = "Requirement List", response = RequirementModel.class)
    public List<RequirementModel> index() {
        return requirementService.findAll();
    }

    @PostMapping(path = "/")
    @ApiOperation(value = "Create new requirement", response = RequirementModel.class)
    public RequirementModel store(@RequestBody RequirementModel project) {
        return requirementService.store(project);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Get requirement by id", response = RequirementModel.class)
    public Optional<RequirementModel> show(@PathVariable(value = "id") Long id) {
        return requirementService.show(id);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Update requirement by id", response = RequirementModel.class)
    public Optional<RequirementModel> update(@PathVariable(value = "id") Long id, @RequestBody RequirementModel requirement) {
        Optional<RequirementModel> requirementToUpdate = requirementService.show(id);

        return requirementToUpdate.isEmpty() ? requirementToUpdate : Optional.ofNullable(requirementService.update(requirement));
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete requirement by id")
    public boolean delete(@PathVariable(value = "id") Long id) {
        int count = requirementService.countRequirementTasks(id);

        return count == 0 ? requirementService.delete(id) : false;
    }
}
