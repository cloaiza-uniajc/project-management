package co.edu.uniajc.project.management.repository;

import co.edu.uniajc.project.management.model.RequirementModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RequirementRepository extends JpaRepository<RequirementModel, Long> {
    @Query(nativeQuery = true, value = "SELECT count(*) FROM tasks WHERE id_requirement = :id")
    int countRequirementTasks(@Param(value = "id") Long id);
}

