package co.edu.uniajc.project.management.controller;

import co.edu.uniajc.project.management.model.TaskModel;
import co.edu.uniajc.project.management.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/task")
@Api("Project")
public class TaskController {
    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(path = "/")
    @ApiOperation(value = "Tasks List", response = TaskModel.class)
    public List<TaskModel> index() {
        return taskService.findAll();
    }

    @PostMapping(path = "/")
    @ApiOperation(value = "Create new task", response = TaskModel.class)
    public TaskModel store(@RequestBody TaskModel project) {
        return taskService.store(project);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Get task by id", response = TaskModel.class)
    public Optional<TaskModel> show(@PathVariable(value = "id") Long id) {
        return taskService.show(id);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Update task by id", response = TaskModel.class)
    public Optional<TaskModel> update(@PathVariable(value = "id") Long id, @RequestBody TaskModel project) {
        Optional<TaskModel> taskToUpdate = taskService.show(id);

        return taskToUpdate.isEmpty() ? taskToUpdate : Optional.ofNullable(taskService.update(project));
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete task by id")
    public boolean delete(@PathVariable(value = "id") Long id) {
        Optional<TaskModel> projectToDelete = taskService.show(id);

        if (!projectToDelete.isEmpty()) {
            taskService.delete(id);
            return true;
        }

        return false;
    }
}
