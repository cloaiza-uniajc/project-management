CREATE SCHEMA IF NOT EXISTS project_management;

CREATE TABLE IF NOT EXISTS project_management.projects
(
    id           integer                NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name         character varying(255) NOT NULL,
    client       character varying(255) NOT NULL,
    start_date   date                   NOT NULL,
    final_date   date                   NOT NULL,
    status       character varying(255) NOT NULL,
    observations character varying(255),
    CONSTRAINT project_pkey PRIMARY KEY ("id")
)
    TABLESPACE pg_default;

ALTER TABLE project_management.projects
    OWNER to test_user;

GRANT ALL ON TABLE project_management.projects TO test_user;

CREATE TABLE IF NOT EXISTS project_management.requirements
(
    id           integer                NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    id_project   integer                NOT NULL,
    name         character varying(255) NOT NULL,
    description  character varying(255) NOT NULL,
    final_date   date                   NOT NULL,
    priority     character varying(255) NOT NULL,
    status       character varying(255) NOT NULL,
    observations character varying(255),
    CONSTRAINT requirements_pkey PRIMARY KEY ("id"),
    CONSTRAINT "FK_REQUIREMENTS" FOREIGN KEY (id_project)
        REFERENCES project_management.projects (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
    TABLESPACE pg_default;

ALTER TABLE project_management.requirements
    OWNER to test_user;

GRANT ALL ON TABLE project_management.requirements TO test_user;

CREATE TABLE IF NOT EXISTS project_management.users
(
    id              integer                NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    document_number integer                NOT NULL,
    first_name      character varying(255) NOT NULL,
    last_name       character varying(255) NOT NULL,
    email           character varying(255) NOT NULL,
    password        character varying(255) NOT NULL,
    status          character varying(255) NOT NULL,
    role            character varying(255) NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY ("id")
)
    TABLESPACE pg_default;

ALTER TABLE project_management.users
    OWNER to test_user;

GRANT ALL ON TABLE project_management.users TO test_user;

CREATE TABLE IF NOT EXISTS project_management.tasks
(
    id             integer                NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    id_requirement integer                NOT NULL,
    name           character varying(255) NOT NULL,
    description    character varying(255) NOT NULL,
    final_date     date                   NOT NULL,
    priority       character varying(255) NOT NULL,
    status         character varying(255) NOT NULL,
    observations   character varying(255),
    id_responsible integer,
    CONSTRAINT tasks_pkey PRIMARY KEY ("id"),
    CONSTRAINT "FK_TASKS" FOREIGN KEY (id_requirement)
        REFERENCES project_management.requirements ("id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "FK_TASKS_USERS" FOREIGN KEY (id_responsible)
        REFERENCES project_management.users ("id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
    TABLESPACE pg_default;

ALTER TABLE project_management.tasks
    OWNER to test_user;

GRANT ALL ON TABLE project_management.tasks TO test_user;