package co.edu.uniajc.project.management;

import co.edu.uniajc.project.management.controller.RequirementController;
import co.edu.uniajc.project.management.model.RequirementModel;
import co.edu.uniajc.project.management.service.RequirementService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
class RequirementUnitTest {

    RequirementService requirementServiceMock = Mockito.mock(RequirementService.class);

    RequirementController requirementController = new RequirementController(requirementServiceMock);

    RequirementModel requirementModel = new RequirementModel();

    private SimpleDateFormat formato;

    @BeforeEach
    void setUp() {

        formato = new SimpleDateFormat("yyyy-mm-dd");
        requirementModel.setId(2L);
        requirementModel.setIdProject(3L);
        requirementModel.setName("Lorem");
        requirementModel.setDescription("Ipsum Lorem");
        try {
            requirementModel.setFinalDate(formato.parse("2021-09-10"));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("error try catch");
        }
        requirementModel.setPriority("Alta");
        requirementModel.setStatus("Pendiente");
        requirementModel.setObservations("Observación Lorem Ipsum");
    }

    @Test
    void index() {
        Mockito.when(requirementServiceMock.findAll()).thenReturn(Arrays.asList(requirementModel));
        requirementController.index();
    }

    @Test
    void store() {
        Mockito.when(requirementServiceMock.store(any(RequirementModel.class))).thenReturn(requirementModel);
        requirementController.store(any());
    }

    @Test
    void show() {
        Mockito.when(requirementServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(requirementModel));
        requirementController.show(any());
    }

    @Test
    void update() {
        Mockito.when(requirementServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(requirementModel));
        Mockito.when(requirementServiceMock.update(any(RequirementModel.class))).thenReturn(requirementModel);
        requirementController.show(any());
        requirementController.update(any(), requirementModel);
    }

    @Test
    void updateEmpty() {
        Mockito.when(requirementServiceMock.show(any())).thenReturn(Optional.empty());
        Mockito.when(requirementServiceMock.update(any(RequirementModel.class))).thenReturn(requirementModel);
        requirementController.show(any());
        requirementController.update(any(), requirementModel);
    }

    @Test
    void delete() {
        Mockito.when(requirementServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(requirementModel));
        requirementController.delete(any());
    }
}