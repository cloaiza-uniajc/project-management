package co.edu.uniajc.project.management.service;

import co.edu.uniajc.project.management.model.ProjectModel;
import co.edu.uniajc.project.management.model.UserModel;
import co.edu.uniajc.project.management.repository.ProjectRepository;
import co.edu.uniajc.project.management.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    private UserModel userModel;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        userModel = new UserModel();
        userModel.setId(123L);
        userModel.setEmail("correo@prueba.com");
        userModel.setStatus("Activo");
        userModel.setDocumentNumber(1225114);
        userModel.setFirstName("Juan");
        userModel.setLastName("Prueba");
        userModel.setPassword("125abc");
        userModel.setRole("Equipo");
    }

    @Test
    void findAll() {
        when(userRepository.findAll()).thenReturn(Arrays.asList(userModel));
        assertNotNull(userService.findAll());
    }

    @Test
    void store() {
        when(userRepository.save(any(UserModel.class))).thenReturn(userModel);
        assertNotNull(userService.store(userModel));
    }

    @Test
    void show() {
        when(userRepository.findAllById(any())).thenReturn(Arrays.asList(userModel));
        assertNotNull(userService.show(any()));
    }

    @Test
    void update() {
        when(userRepository.save(any(UserModel.class))).thenReturn(userModel);
        assertNotNull(userService.update(userModel));
    }

    @Test
    void delete() {
        Mockito.when(userRepository.findAllById(any())).thenReturn(Arrays.asList(userModel));
        userService.delete(any());
    }


    @Test
    void getByEmail() {
        Mockito.when(userRepository.findByEmail(any())).thenReturn(java.util.Optional.ofNullable(userModel));
        userService.findByEmail(any());
    }


    @Test
    void countUserTask() {
        when(userRepository.findAllById(any())).thenReturn(Arrays.asList(userModel));
        assertNotNull(userService.countUserTasks(any()));
    }
}