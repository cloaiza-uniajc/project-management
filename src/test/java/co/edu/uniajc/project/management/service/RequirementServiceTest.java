package co.edu.uniajc.project.management.service;

import co.edu.uniajc.project.management.model.RequirementModel;
import co.edu.uniajc.project.management.repository.RequirementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RequirementServiceTest {

    @Mock
    private RequirementRepository requirementRepository;

    @InjectMocks
    private RequirementService requirementService;

    private RequirementModel requirementModel;

    private SimpleDateFormat formato;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        formato = new SimpleDateFormat("yyyy-mm-dd");
        requirementModel = new RequirementModel();
        requirementModel.setId(2L);
        requirementModel.setIdProject(3L);
        requirementModel.setName("Lorem");
        requirementModel.setDescription("Ipsum Lorem");
        try {
            requirementModel.setFinalDate(formato.parse("2021-09-10"));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("error try catch");
        }
        requirementModel.setPriority("Alta");
        requirementModel.setStatus("Pendiente");
        requirementModel.setObservations("Observación Lorem Ipsum");
    }

    @Test
    void findAll() {
        when(requirementRepository.findAll()).thenReturn(Arrays.asList(requirementModel));
        assertNotNull(requirementService.findAll());
    }

    @Test
    void store() {
        when(requirementRepository.save(any(RequirementModel.class))).thenReturn(requirementModel);
        assertNotNull(requirementService.store(requirementModel));
    }

    @Test
    void show() {
        when(requirementRepository.findAllById(any())).thenReturn(Arrays.asList(requirementModel));
        assertNotNull(requirementService.show(any()));
    }

    @Test
    void update() {
        when(requirementRepository.save(any(RequirementModel.class))).thenReturn(requirementModel);
        assertNotNull(requirementService.update(requirementModel));
    }

    @Test
    void delete() {
        Mockito.when(requirementRepository.findAllById(any())).thenReturn(Arrays.asList(requirementModel));
        requirementService.delete(any());
    }

    @Test
    void countRequirementTasks() {
        when(requirementRepository.findAllById(any())).thenReturn(Arrays.asList(requirementModel));
        assertNotNull(requirementService.countRequirementTasks(any()));
    }
}