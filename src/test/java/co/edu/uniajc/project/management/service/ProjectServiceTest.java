package co.edu.uniajc.project.management.service;

import co.edu.uniajc.project.management.model.ProjectModel;
import co.edu.uniajc.project.management.repository.ProjectRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class ProjectServiceTest {

    @Mock
    private ProjectRepository projectRepository;

    @InjectMocks
    private ProjectService projectService;

    private ProjectModel projectModel;

    private SimpleDateFormat formato;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        formato = new SimpleDateFormat("yyyy-mm-dd");
        projectModel = new ProjectModel();
        projectModel.setId(1L);
        projectModel.setName("Gestion de registros proyectos nuevos");
        projectModel.setClient("Laura sofia");
        try {
            projectModel.setStartDate(formato.parse("2020-09-05"));
            projectModel.setFinalDate(formato.parse("2021-10-31"));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("error try catch");
        }
        projectModel.setStatus("Activo");
        projectModel.setObservations("El proyecto contará con 2 revisiones por mes");
    }

    @Test
    void findAll() {
        when(projectRepository.findAll()).thenReturn(Arrays.asList(projectModel));
        assertNotNull(projectService.findAll());
    }

    @Test
    void store() {
        when(projectRepository.save(any(ProjectModel.class))).thenReturn(projectModel);
        assertNotNull(projectService.store(projectModel));
    }

    @Test
    void show() {
        when(projectRepository.findAllById(any())).thenReturn(Arrays.asList(projectModel));
        assertNotNull(projectService.show(any()));
    }

    @Test
    void update() {
        when(projectRepository.save(any(ProjectModel.class))).thenReturn(projectModel);
        assertNotNull(projectService.update(projectModel));
    }

    @Test
    void delete() {
        Mockito.when(projectRepository.findAllById(any())).thenReturn(Arrays.asList(projectModel));
        projectService.delete(any());
    }

    @Test
    void countProjectRequirements() {
        when(projectRepository.findAllById(any())).thenReturn(Arrays.asList(projectModel));
        assertNotNull(projectService.countProjectRequirements(any()));
    }
}