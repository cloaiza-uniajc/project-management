package co.edu.uniajc.project.management.service;

import co.edu.uniajc.project.management.model.TaskModel;
import co.edu.uniajc.project.management.repository.TaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class TaskServiceTest {
    @Mock
    private TaskRepository taskRepository;

    @InjectMocks
    private TaskService taskService;


    private TaskModel taskModel;

    private SimpleDateFormat formato;

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);
        formato = new SimpleDateFormat("yyyy-mm-dd");
        taskModel = new TaskModel();
        taskModel.setId(1L);
        taskModel.setIdRequirement(1L);
        taskModel.setName("prueba");
        taskModel.setDescription("Esto es una prueba");
        try {
            taskModel.setFinalDate(formato.parse("2021-09-08"));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("error try catch");
        }
        taskModel.setPriority("Normal");
        taskModel.setStatus("procesando");
        taskModel.setObservations("Prueba observación");
        taskModel.setIdResponsible(1L);
    }

    @Test
    void findAll() {
        when(taskRepository.findAll()).thenReturn(Arrays.asList(taskModel));
        assertNotNull(taskService.findAll());
    }

    @Test
    void store() {
        when(taskRepository.save(any(TaskModel.class))).thenReturn(taskModel);
        assertNotNull(taskService.store(taskModel));
    }

    @Test
    void show() {
        when(taskRepository.findAllById(any())).thenReturn(Arrays.asList(taskModel));
        assertNotNull(taskService.show(any()));
    }

    @Test
    void update() {
        when(taskRepository.save(any(TaskModel.class))).thenReturn(taskModel);
        assertNotNull(taskService.update(taskModel));
    }

    @Test
    void delete() {
        Mockito.when(taskRepository.findAllById(any())).thenReturn(Arrays.asList(taskModel));
        taskService.delete(any());
    }
}