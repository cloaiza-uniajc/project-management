package co.edu.uniajc.project.management;

import co.edu.uniajc.project.management.controller.UserController;
import co.edu.uniajc.project.management.model.UserModel;
import co.edu.uniajc.project.management.repository.UserRepository;
import co.edu.uniajc.project.management.service.ProjectService;
import co.edu.uniajc.project.management.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserUnitTest {

    @Mock
    private UserService userService;

    UserService userServiceMock = Mockito.mock(UserService.class);
    UserController userController = new UserController(userServiceMock);
    UserModel userModel = new UserModel();

    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }


    @Test
    void index() {
        Mockito.when(userService.findAll()).thenReturn(Arrays.asList(userModel));
        userController.index();
    }

    @Test
    void store() {
        Mockito.when(userService.store(any(UserModel.class))).thenReturn(userModel);
        userController.store(any());
    }

    @Test
    void show() {
        Mockito.when(userService.show(any())).thenReturn(java.util.Optional.ofNullable(userModel));
        userController.show(any());
    }

    @Test
    void update() {
        Mockito.when(userService.show(any())).thenReturn(java.util.Optional.ofNullable(userModel));
        Mockito.when(userService.update(any(UserModel.class))).thenReturn(userModel);
        userController.show(any());
        userController.update(any(), userModel);
    }


    @Test
    void getByEmail() {
        Mockito.when(userService.findByEmail(any())).thenReturn(java.util.Optional.ofNullable(userModel));
        userController.getByEmail(any());
    }

    @Test
    void updateEmpty() {
        Mockito.when(userService.show(any())).thenReturn(Optional.empty());
        Mockito.when(userService.update(any(UserModel.class))).thenReturn(userModel);
        userController.show(any());
        userController.update(any(), userModel);
        Optional<UserModel> projectToUpdate = userService.show(any());

        final var userModel = projectToUpdate.isEmpty() ? projectToUpdate : Optional.ofNullable(userService.update(this.userModel));
    }

    @Test
    void delete() {
        Mockito.when(userService.show(any())).thenReturn(java.util.Optional.ofNullable(userModel));
        Optional<UserModel> projectToDelete = userService.show(any());

        if (!projectToDelete.isEmpty()) {
            userService.delete(any());
            userController.delete(any());
        }

    }
}


