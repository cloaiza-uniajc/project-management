package co.edu.uniajc.project.management;

import co.edu.uniajc.project.management.controller.TaskController;
import co.edu.uniajc.project.management.model.TaskModel;
import co.edu.uniajc.project.management.model.UserModel;
import co.edu.uniajc.project.management.service.TaskService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

class TaskUnitTest {

    @Mock
    TaskService taskServiceMock;

    @InjectMocks
    TaskController taskController;

    TaskModel taskModel;

    private SimpleDateFormat formato;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        formato = new SimpleDateFormat("yyyy-mm-dd");
        taskModel = new TaskModel();
        taskModel.setId(1L);
        taskModel.setIdRequirement(5L);
        taskModel.setName("prueba");
        taskModel.setDescription("Esto es una prueba");
        try {
            taskModel.setFinalDate(formato.parse("2021-09-08"));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("error try catch");
        }
        taskModel.setPriority("Normal");
        taskModel.setStatus("procesando");
        taskModel.setObservations("Prueba observación");
        taskModel.setIdResponsible(1L);
    }

    @Test
    void index() {
        Mockito.when(taskServiceMock.findAll()).thenReturn(Arrays.asList(taskModel));
        assertNotNull(taskController.index());
    }

    @Test
    void store() {
        Mockito.when(taskServiceMock.store(any(TaskModel.class))).thenReturn(taskModel);
        assertNotNull(taskController.store(new TaskModel()));

    }

    @Test
    void show() {
        Mockito.when(taskServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(taskModel));
        assertNotNull(taskController.show(taskModel.getId()));
    }

    @Test
    void update() {
        Mockito.when(taskServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(taskModel));
        Mockito.when(taskServiceMock.update(any(TaskModel.class))).thenReturn(taskModel);
        assertNotNull(taskController.show(taskModel.getId()));
        assertNotNull(taskController.update(taskModel.getId(), taskModel));
    }

    @Test
    void updateEmpty() {
        Mockito.when(taskServiceMock.show(any())).thenReturn(Optional.empty());
        Mockito.when(taskServiceMock.update(any(TaskModel.class))).thenReturn(taskModel);
        assertNotNull(taskController.show(taskModel.getId()));
        assertNotNull(taskController.update(taskModel.getId(), taskModel));
    }

    @Test
    void delete() {
        Mockito.when(taskServiceMock.show(any())).thenReturn(java.util.Optional.ofNullable(taskModel));
        assertNotNull(taskController.delete(any()));
    }

    @Test
    void deleteNoExists() {
        Mockito.when(taskServiceMock.show(any())).thenReturn(Optional.empty());
        assertNotNull(taskController.delete(any()));
    }
}